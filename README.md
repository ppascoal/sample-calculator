
  <p align="center">A progressive <a href="http://nodejs.org" target="_blank">Node.js</a> framework for building efficient and scalable server-side applications.</p>
    <p align="center">
<a href="https://www.npmjs.com/~nestjscore" target="_blank"><img src="https://img.shields.io/npm/v/@nestjs/core.svg" alt="NPM Version" /></a>
<a href="https://www.npmjs.com/~nestjscore" target="_blank"><img src="https://img.shields.io/npm/l/@nestjs/core.svg" alt="Package License" /></a>
<a href="https://circleci.com/gh/nestjs/nest" target="_blank"><img src="https://img.shields.io/circleci/build/github/nestjs/nest/master" alt="CircleCI" /></a>
</p>

## Description

Sample and simple calculator using [Nest](https://github.com/nestjs/nest)

## Installation

```bash
$ npm install
```

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

## Test

```bash
# unit tests
$ npm run test

# e2e tests
$ npm run test:e2e

# test coverage
$ npm run test:cov
```
## Code sample
```JavaScript
getCalculator(a: number, b: number, op: string): any {
    const operations = {
      adicionar: function (a, b) {
        return +a + +b;
      },
      subtrair: function (a, b) {
        return a - b;
      },
      multiplicar: function (a, b) {
        return a * b;
      },
      dividir: function (a, b) {
        return a / b;
      },
    };

    const operation = operations[op];

    return {
      result: operation(a, b),
    };
  }
```

## License

[MIT licensed](LICENSE).
