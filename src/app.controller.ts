import { Body, Controller, Get, Param, Post } from "@nestjs/common";
import { AppService } from './app.service';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get(':a&:b&:op')
  getCalculator(@Param() params): string {
    console.log('params', params);
    const { a, b, op } = params;
    return this.appService.getCalculator(Number(a), Number(b), op);
  }

}
