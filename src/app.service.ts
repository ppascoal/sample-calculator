import { Injectable } from '@nestjs/common';

// define result

@Injectable()
export class AppService {
  getCalculator(a: number, b: number, op: string): any {
    const operations = {
      adicionar: function (a, b) {
        return +a + +b;
      },
      subtrair: function (a, b) {
        return a - b;
      },
      multiplicar: function (a, b) {
        return a * b;
      },
      dividir: function (a, b) {
        return a / b;
      },
    };

    const operation = operations[op];

    return {
      result: operation(a, b),
    };
  }
}
